# Ring signatures. An implementation of undeniable and ring signatures scheme based on RSA. 

### Commands:

	bin/create_keys <number-of-keys : int>
	bin/create_signature <public-key-directory : string> <private-key-file : string> <file-to-sign : string> <output-file : string>
	bin/verify_signature <file : string> <signature-of-file : string> 

### Example:

	bin/create_keys 500
	bin/create_signature keys/public/ keys/private/347.sk Makefile first
	bin/verify_signature Makefile signatures/first.sig

### Install:

	install zlib1g-dev
	install pkg-config package
	install rng-tools
	install ocaml
	install opam
	eval $(opam config env)

	opam init
	opam update
	opam install cryptokit 
	opam install yojson
	opam install ANSITerminal

### Compile:

	make