\documentclass[10pt]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=3cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage{minted}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{float}
\selectlanguage{english}
\usepackage{listings}
\usepackage{indentfirst}

\setlength{\parindent}{4em}
\setlength{\parskip}{0em}
\renewcommand{\baselinestretch}{1.2}

%% Title
\title{
	%\vspace{-1in} 	
	\usefont{OT1}{bch}{b}{n}
	\includegraphics[scale=0.25]{Image/IST_LOGO.jpg} \\ 
	\normalfont \normalsize 
	\huge A Ring Signature Implementation \\
}

\usepackage{authblk}
\author[0]{Ricardo Ciríaco da Graça  - ${93994}$}

\begin{document}
	\maketitle
	
	\section{Introduction}
	
	Sometimes, people want to leak authoritative secrets without revealing their identity. Although a group signature scheme, that has a trusted identity, may solve this problem, under certain circumstances (e.g., bribes or threats), it is not reliable to trust a person with our anonymity. A ring signature scheme, which does not depend on any trusted authority, prevents anyone but himself from revoking his anonymity and still make sure that the message's source is legitimate. 
	
	This report will serve as a detailed step-by-step guide on how to implement a ring signature based on RSA. It uses the same steps refered in the "How to leak a secret" paper \cite{rivest2001leak} for both signature and verification method. It will also include a small manual on how to use the system in order to create and verify a ring signature. All the code created trough out the implementation, can be seen in the \href{https://gitlab.com/Ricagraca/ringsignature}{public git repository}.
	
	\newpage
	
	\section{Implementation}
	
	\subsection{Extending trap-door permutations to a common domain}
	
	Every ring member has an RSA public key $P_i = (n_i, e_i)$ and can encrypt a value by using the trapdoor one-way permutation $f_i$ of $\mathbf{Z_{n_i}}$. 
	
	\begin{center}
		$f_i(x) = x^{e_i} $ (mod $n_i$)
	\end{center}

	Since the values of $n$ are different for each member in the ring, each trap-door function $f_i$ is extended to a $g_i$ function so that the domain of all $g_i$ functions are the same, $\{0,1\}^b$. The function $g_i$ receives a message $m = q_i n_i + r_i$ and is computed as follows.
	
	\begin{center}
		\[ 
		g_i(m)= \left\{
		\begin{array}{ll}
		q_i n_i + f_i (r_i) & \text{ if } (q_i + 1)n_i \leq 2^b \\
		m & \text{ else } \\
		\end{array} 
		\right. 
		\]
	\end{center}

\bigskip

\begin{minted}{ocaml}
		let nq_n_ = add nq_ n_ in 
		let in_range = CryptokitBignum.compare nq_n_ pow_2_b <= 0 in 
		
		if in_range then 
			let f  = encrypt pub_key r in 
			let f_ = of_bytes f in  
			to_bytes (add nq_ f_)
		else
			m
\end{minted}

	By the given definition, $g_i$ can be trivially inverted, ${g_i}^{-1}$, assuming that the user has access to the private key $S_i=(n_i,d_i)$, in order to compute ${{f_i}^{-1}}(r_i)$. Considering that $y = q_i n_i + r_i$, one can compute ${g_i}^{-1}(y)$ as follows.
	
	\begin{center}
		\[ 
		{g_i}^{-1}(y)= \left\{
		\begin{array}{ll}
		q_i n_i + {{f_i}^{-1}}(r_i) & \text{ if } (q_i + 1)n_i   \leq 2^b \\
		y & \text{ else } \\
		\end{array} 
		\right. 
		\]
	\end{center}	

	\bigskip
	
\begin{minted}{ocaml}
	  let nq_n_ = add nq_ n_ in 
	  let in_range = CryptokitBignum.compare nq_n_ pow_2_b <= 0 in 
	
	  if in_range then 
		  let f_inv_r  = decrypt priv_key (to_bytes f_r_) in 
		  let f_inv_r_ = of_bytes f_inv_r in 
		  add nq_ f_inv_r_
	  else 
		  y_
\end{minted}

	\newpage
	
	\subsection{Combining function}
	\label{subsection:comb}
	Assume the following combining function $C_{k,v}(y_1,y_2,...,y_r)$:
	
	\begin{center}
		$C_{k,v}(y_1,y_2,...,y_r) = E_k(y_r \oplus E_k(y_{r-1} \oplus E_k(y_{r-2} \oplus E_k(\cdots \oplus E_k(y_{1}  \oplus v )\cdots ))))$
	\end{center}
	
	This function uses a symmetric encryption function $E$ and takes $k$ as key, $E_k$. The glue value $v$ is just a random value in the same codomain of $g$. During the implementation, it was used AES-256 with the self-synchronizing CFB-8 mode as the encryption function $E$. For the $k$ it was used the SHA-256 value of the message being signed, since it is considered a collision-resistant hash function. If $E$ is used as the identity function, it is possible to use linear algebra when $r$ is large enough to find a solution to the equation in figure \ref{fig:combining-function} \cite{rivest2001leak}. 
	
	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.45]{Image/signature.png}
		\caption{Illustration of the combining function}
		\label{fig:combining-function}
	\end{figure}
	
	One of the properties of the combining function, taken from the "How to leak a secret" paper \cite{rivest2001leak}, is the following:
	
	\begin{flushleft}
		\textbf{Permutation on each input:} For each $s$, $1 \leq s \leq r$, given a $b$-bit value $z$ and values for all inputs $y_i$ except $y_s$, it is possible to efficiently find a $b$-bit value for $y_s$ such that $C_{k,v}(y_1,y_2,...,y_r)=z$.
	\end{flushleft}

	To solve the equation $C_{k,v}(y_1,y_2,...,y_r)=z$, one can  apply successively $E^{-1}_k$ and XOR of $y_i$, to both sides of the equation, until it reaches $y_s$, getting rid of the function application to the left of $y_s$. Then it stays with a equation of the form: $y_s \oplus E_k(\cdots \oplus E_k(y_{1}  \oplus v )\cdots ) = y_{s+1} \oplus E^{-1}_k(\cdots v \cdots)$. Since $E_k(\cdots \oplus E_k(y_{1}  \oplus v )\cdots )$ , the right side of $y_s$ is easily computable by applying XOR of $y_i$ and $E_k$, then $y_s = (E_k(\cdots \oplus E_k(y_{1}  \oplus v )\cdots ) \oplus (y_{s+1} \oplus E^{-1}_k(\cdots v \cdots))$. We can compute all of this efficiently, thus satisfying the property above.

	\bigskip

	\begin{minted}{ocaml}
	let compute_y (...) = 
		(...)
		let u = e' glue in (* Decrypt glue value E^(-1)(glue)*)
		let left_value = compute_left (number_of_users-1) u in 
		let right_value = compute_right 0 glue in 
		let y_s = xor_string left_value right_value in 
	\end{minted}
	
	\newpage
	
	\subsection{Signature method}
	
	\begin{flushleft}
	\textbf{1. Choose a key: $k = h(m)$ : } 
	
	\bigskip 
	\hspace{0.4cm} Load file into memory and get its SHA-256 value to use it as a key $k$ in $E_k$.
	
	\begin{minted}{ocaml}
	let k = hash_of_file file in 
	\end{minted}

	\bigskip 

	\textbf{2. Pick a random glue value : } 
	
	\bigskip 
	\hspace{0.4cm} Get a b-bit random number from the pseudorandom number generator file "/dev/urandom".

	\begin{minted}{ocaml}
	let v = rand b in
	\end{minted}
	
	\bigskip 
	
	\textbf{3. Pick random $x_i$'s and compute $y_i = g_i(x_i)$ for $i \neq u$ :}
	
	\bigskip 
	\hspace{0.4cm} Generate an array $X$ of b-bit random numbers and apply $g_i = g(P_i)$ to each $x_i$ of $X$  to get $Y$.
	
	\hspace{0.4cm} Except for the user $u$, creating the signature, i.e., $x_u = y_u = \text{NULL}$.
	
	\begin{minted}{ocaml}
	let x = Array.init ring_size (
		fun i -> if i != u then rand b else bzero
	) in 
	
	let y = Array.mapi (
		fun i xi -> if i != u then g i xi else ""
	) x in 
	\end{minted}
	
	\bigskip 
	
	\textbf{4. Solve $C_{k,v}(y_1,y_2,...,y_r) = v$ for $y_u$ :}
	
	\bigskip 
	\hspace{0.4cm} Compute $y_u$ using the array $y$, key $k$ and glue value $v$, calculated in the previous steps.
	
	\begin{minted}{ocaml}
	let y_u = compute_y_u y k v in 
	\end{minted}
	
	\bigskip 
	
	\textbf{5. Invert the signer’s trap-door permutation :}
	
	\bigskip 
	\hspace{0.4cm} For this step it is needed the secret key of user $u$, $S_u$, to invert $y_u$ and get $x_u$, $x_u = g^{-1}(y_u)$.
	
	\hspace{0.4cm} And store $x_u$ in index $u$ of array $X$. 

	\begin{minted}{ocaml}
	let x_u = g' s_k y_u in 
	x.(u) <- x_u;
	\end{minted}
	
	\textbf{6. Output the ring signature :}
	
	\bigskip 
	\hspace{0.4cm} The output consists of a tuple of all the public keys, the glue value and the $x$ array.
	
	\hspace{0.4cm} Signature method: $\text{signature}(P_1, P2,..., P_r, S_u, file) = (P_1,P2,...,P_r, v, X)$.
	
	\begin{minted}{ocaml}
	(pks,v,x)
	\end{minted}
	
	\end{flushleft}

	\newpage
\newpage

\subsection{Verify method}

\begin{flushleft}
	\textbf{1. Apply the trap-door permutations: } 
	
	\bigskip 
	\hspace{0.4cm} Apply each $g_i$ to $x_i$.
	
	\begin{minted}{ocaml}
	let y = Array.mapi (fun i e -> g i e) x in 
	\end{minted}
	
	\bigskip 
	
	\textbf{2. Obatin k: } 

	\bigskip 
	\hspace{0.4cm} Get the SHA-256 value of the file.
	
	\begin{minted}{ocaml}
	let key = hash_of_file file_name in 
	\end{minted}

	\textbf{3. Verify the ring signature: } 

	\bigskip 
	
	\hspace{0.4cm} Apply XOR of $y_i$ and $E_k$, just like explained in section \ref{subsection:comb}, to verify if $C_{k,v}(y_1,y_2,...,y_r) = v$
	
	\begin{minted}{ocaml}
	let satisfies = ring_equality pks y glue key in 
	\end{minted}

\bigskip 

\end{flushleft}
	
	\newpage 



	\section{Manual}
	All the necessary steps for the installation are on the "./README.md" file under the root "./" of the project. After following the steps in the file, the user should have three programs inside the "./bin/" folder. The files outputted by these programs are written in json format so they can be easily readen by any application.
	
	\begin{itemize}
		\item ./bin/create\_keys
		\item ./bin/create\_signature
		\item ./bin/verify\_signature
	\end{itemize}
	
	\subsection{Create keys}
	In order to test the ring signature implementation, there is a program "./bin/create\_keys" to generate several RSA keys. The program takes as input the number of keys the user wants to generate and outputs a folder of keys "./keys/" containing a public key folder "./keys/public/" with all the public keys, and a private key folder "./keys/private/" with the respective private keys. 

	\bigskip

	\begin{lstlisting}
			  # Create 100 RSA keys
			  ./bin/create_keys 100
	\end{lstlisting}
	
	\subsection{Create ring signature}
	The "./bin/create\_signature" program, creates a ring signature. It takes as input, one folder of the public keys (including the users public key) "./keys/public", the private key of the user "./keys/private/\{$u$\}.sk", the file that user $u$ wants to sign and the name of the outputted signature. The signature is saved unto the signatures folder "./signatures".
	
	\bigskip
	
	\begin{lstlisting}[basicstyle=\small]
# User 50 signing file RST01.pdf, outputs my-first.sig to ./signatures folder
./bin/create_signature ./keys/public ./keys/private/050.sk RST01.pdf my-first
	\end{lstlisting}
	
	\subsection{Verify ring signature}
	Finally, to verify the ring signature, it is used ".bin/verify\_signature". It takes as input the file that was signed and the generated signature. It outputs "Accepted" if the signature is valid in regards to the inputed file, or "Rejected" when the signature doesn't correspond to the inputted file.

	\bigskip

\begin{lstlisting}[basicstyle=\small]
 	 # Verifying  the  signature  generated  by create_signature
	 ./bin/verify_signature  RST01.pdf ./signatures/my-first.sig
 	 Accepted
	
	 # Using  a different file will output Rejected as  expected
	 ./bin/verify_signature  RST02.pdf ./signatures/my-first.sig
 	 Rejected
\end{lstlisting}

	\newpage
	
	\section{Conclusion}
	Using this implementation, we can generate a ring signature out of a public key repository. Anyone that looks at the ring signature, cannot figure out who, in the public key repository, signed it. This is because any one who owns one of the public keys in the signature could of outputted that ring signature and thus every outsider sees every output $y_i$ of the signature as correct. If someone wants to sign, he will need at least one of the private keys correspondent of the public key repository in order to use $g^{-1}$ to solve the combining equation $C_{k,v} = v$ This scheme is being used by several cryptocurrencies such as Monero, in order to protect user's privacy in the input side of a transaction. Although a ring signature can be computed efficiently, it should only be used in particular cases, to avoid the extra cost of the computation. 

	\newpage

	\bibliographystyle{unsrt}
	\bibliography{bibliography}
	
\end{document}