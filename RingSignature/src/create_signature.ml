
	open Printf
	open Sys
	open Signature
	open Cryptokit 
	open RSA 
	open CryptokitBignum 
	open Cryptokit.Random
	open Utility

	let program_name = Sys.argv.(0)
	let no_input_message = "Wrong call: "^program_name^" <public-key-directory : string> <private-key-file : string> <file-to-sign : string> <output-file : string> "

	let _ =
		try 
		(
			let pub_key_dir = Sys.argv.(1) in
			let priv_key_file = Sys.argv.(2) in
			let file_name = Sys.argv.(3) in
			let output_name = Sys.argv.(4) in
			save_signature pub_key_dir priv_key_file file_name output_name
		)
		with 
			| Sys_error(e) -> printf "%s\n" (e);
			| Invalid_argument(e) -> printf "%s\n" (no_input_message);
