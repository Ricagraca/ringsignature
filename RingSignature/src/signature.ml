
	open Cryptokit 
	open List 
	open CryptokitBignum 
	open RSA 
	open String 
	open Random
	open Unix
	open Sys
	open Yojson 
	open Yojson.Basic
	open Yojson.Basic.Util
	open Utility

	(* Folder to store the signatures *)
	let signature_directory = "signatures/"

	(* Using dev/urandom *)
	let device = "/dev/urandom"

	(*If we choose a sufficiently large b (e.g. 160 bi- 
	ts larger than any of the ni), the chance that a ra
	ndomly chosen m is unchanged by the extended gi be-
	comes negligible. *)
	
	(* Size of key in bits *)
	let two = of_int 2 
	let key_size = 1024

	let b = key_size + 160
	let pow_2_b = big_pow_2 b
	let bzero = of_int 0

	(* Get a random number generator that uses /dev/urandom file *)
	let rng = ((device_rng device)#random_bytes) 

	let rand (bit : int) : CryptokitBignum.t = 
		random rng bit


	(* 'm = 'q * 'n + 'r *)
	(* x_ if CryptoBignum, x if string *)

	(* common domain function *)
	let g (pub_key : key) (m_ : CryptokitBignum.t) : string = 
		let {n} = pub_key in 
		let m = to_bytes m_ in 
		let n_ = of_bytes n in 

		(* ri = m mod ni *)
		let r_ = mod_ m_ n_ in 
		let r  = to_bytes r_ in 

		(* m - ri = ni*qi *)
		let nq_ = CryptokitBignum.sub m_ r_ in 

		(* niqi + ni <= 2^b <-> ni (qi + 1) <= 2^b *)
		let nq_n_ = add nq_ n_ in 
		let in_range = CryptokitBignum.compare nq_n_ pow_2_b <= 0 in 
		
		(* ni*qi + f(ri) if in_range else m *) 
		if in_range then 
			let f  = encrypt pub_key r in 
			let f_ = of_bytes f in  
			to_bytes (add nq_ f_)
		else
			m


	(* g' is the inverse of g *)
	let g' (priv_key : key) (y : string) : CryptokitBignum.t = 
		let {n} = priv_key in 
		let y_ = of_bytes y in 
		let n_ = of_bytes n in 

		(* f(ri) = mod ni or ri = m mod ni *)
		let f_r_ = mod_ y_ n_ in 

		(* m - f(ri) = ni*qi or m - ri = ni*qi *)
		let nq_ = CryptokitBignum.sub y_ f_r_ in 

		(* niqi + ni <= 2^b <-> ni (qi + 1) <= 2^b *)
		let nq_n_ = add nq_ n_ in 
		let in_range = CryptokitBignum.compare nq_n_ pow_2_b <= 0 in 
		
		if in_range then 
			let f_inv_r  = decrypt priv_key (to_bytes f_r_) in 
			let f_inv_r_ = of_bytes f_inv_r in 
			add nq_ f_inv_r_
		else 
			y_


	(* Compute y of user *)
	let compute_y (y : string array) (key : string) (v : CryptokitBignum.t) (user : int) : string = 
	
		let e  = aes key Cipher.Encrypt in 
		let e' = aes key Cipher.Decrypt in 
		let glue = to_bytes v in 

		(* Number of users *)
		let number_of_users = Array.length y in  

		(* Compute the left side of the combination function C : follow RST paper *)
		let rec compute_left i acc = 
			match i = user with 
			| true  -> acc
			| false -> compute_left (i-1) (e' (xor_string (y.(i)) acc))
		in 

		(* Compute left r - s times *)
		let u = e' glue in 
		let left_value = compute_left (number_of_users-1) u in 

		(* Compute the right side of the combination function C : follow RST paper *)
		let rec compute_right i acc = 
			match i = user with 
			| true  -> acc
			| false -> compute_right (i+1) (e (xor_string (y.(i)) acc))
		in 		

		(* Compute right s-1 times *)
		let right_value = compute_right 0 glue in 

		let y_s = xor_string left_value right_value in 

		y_s


	(* Compute y of user *)
	let ring_equality (pub_keys : (int * key) array) (y : string array) (glue : CryptokitBignum.t) (key : string) : bool = 
	
		let aes_encrypt = aes key Cipher.Encrypt in 

		let ring_size = Array.length pub_keys in 
		let glue_to_bytes = to_bytes glue in 

		(* Calculate z *)
		let rec calculate_z i acc = 
		match i = ring_size with 
		| true  -> acc
		| false -> calculate_z (i+1) (aes_encrypt (xor_string (y.(i)) (acc))) in 

		let z = calculate_z 0 glue_to_bytes in 

		let equality = z = glue_to_bytes in 
		equality


	(* Generate a ring signature file = file_name *)
	let ring_sign (pub_key_dir : string) (priv_key_file : string) (file : string) : ((int * key) array) * CryptokitBignum.t * (CryptokitBignum.t array) = 

		(* Load all public keys, user private key and additional variables *)
		let pks = read_pub_keys_of_directory pub_key_dir in 
		let u,sk = read_priv_key_of_file priv_key_file in 
		let ring_size = Array.length pks in 

		(* 1st step: Choose a k=key for Ek ; k = h(m) *)
		let k = hash_of_file file in 
		(* End of step 1 *)

		(* 2nd step: Pick a random glue value *)
		let v = rand b in
		(* End of step 2 *)

		(* 3rd step: Pick random xi’s except for user with priv_key *)
		let x = Array.init ring_size (
			fun i -> if i != u then rand b else bzero
		) in 
		
		(* Apply yi = gi(xi) and get array y *)
		let g i xi = g (snd (pks.(i))) xi in 
		let y = Array.mapi (
			fun i xi -> if i != u then g i xi else ""
		) x in 
		(* End of step 3 *)

		(* 4th step: Solve for ys where s = user *)
		let compute_y_u y k v = compute_y y k v u in 
		let y_u = compute_y_u y k v in 
		(* End of step 4 *)

		(* 5th step: Invert the signer’s trap-door permutation *)
		let x_u = g' sk y_u in 
		x.(u) <- x_u;
		(* End of step 5 *)

		(* 6th step: Output the ring signature *)
		(pks,v,x)


	(* Verify ring signature of a file *)
	(* ((int * key) array) * CryptokitBignum.t * (CryptokitBignum.t array) ; m *)
	let ring_verify (file : string) (signature : ((int * key) array) * CryptokitBignum.t * (CryptokitBignum.t array)) : bool =
		
		(* Load *)
		let (pks,glue,x) = signature in 
		
		(* 1st step: Apply the trap-door permutations *)
		let g i xi = g (snd (pks.(i))) xi in 
		let y = Array.mapi (fun i e -> g i e) x in 
		(* End of step 1 *)

		(* 2nd step: Obtain k *)
		let key = hash_of_file file in 
		(* End of step 2 *)

		(* 3nd step: Verify the ring equation *)
		let satisfies = ring_equality pks y glue key in 
		(* End of step 3 *)
		
		satisfies


	(* Save signature to file *)
	let save_signature (pub_key_dir : string) (priv_key_file : string) (file_name : string) (output_name : string) : unit = 

		(* Calculate signature *)
		let (pub_keys,glue,x) = ring_sign pub_key_dir priv_key_file file_name in 

		(* Compress all to json file *)
		let glue_to_bytes = to_bytes glue in 
		let glue_json = `String glue_to_bytes in 
		
		(* Treat jsoning public keys *)
		let pub_keys_list = array_to_list (fun (i,k) -> json_of_pub k i) pub_keys in 
		let pub_keys_json = `List pub_keys_list in
		
		(* Treat jsoning x *)
		let x_list = array_to_list (fun e -> `String (to_bytes e)) x in 
		let x_json = `List x_list in 

		(* Json the final signature *)
		let signature_json = `List [pub_keys_json; glue_json; x_json] in 

		(* Save to output file *)
		if not (file_exists signature_directory) then (
			mkdir signature_directory 0o777
		);

		let signame = concat "." [signature_directory^output_name;"sig"] in 
		Yojson.Basic.to_file signame signature_json 


	(* load to a proper signature *)
	let load_signature (file_name : string) : ((int * key) array) * CryptokitBignum.t * (CryptokitBignum.t array) = 

		let json_of_file = from_file file_name in
		
		let signature  = 
			match json_of_file with 
			| `List l ->  l
			|  _      -> raise (Failure "ERROR")
		in 

		let pub_keys_json, glue_json, x_json = 
		(
			try 
				nth signature 0, nth signature 1, nth signature 2 
			with 
				| Failure(e) -> raise (Failure "ERROR") 
		) in 

		(* list of public keys *)
		let pub_keys_list =
			match pub_keys_json with 
			| `List p -> List.map (fun e -> (read_pub_key e)) p 
			|  _      -> raise (Failure "ERROR")
		in 

		(* glue as a CryptokitBignum.t *)
		let glue =
			match glue_json with 
			| `String g -> of_bytes g
			|  _        -> raise (Failure "ERROR")
		in 

		(* x list of CryptokitBignum.t  *)
		let x_list =
			match x_json with 
			| `List x -> List.map (fun e -> of_bytes (to_string e)) x
			|  _        -> raise (Failure "ERROR")
		in 		

		let pub_key_array = Array.of_list pub_keys_list in 
		let x_array = Array.of_list x_list in 

		(pub_key_array, glue, x_array)




		
