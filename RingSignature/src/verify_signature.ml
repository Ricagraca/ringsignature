
	open ANSITerminal 
	open Printf
	open Sys
	open Signature
	open Cryptokit 
	open RSA 
	open CryptokitBignum 
	open Cryptokit.Random
	open Utility

	(* Example of command *)
	(* bin/verify_signature src/.DS_Store first.sig *)


	let program_name = Sys.argv.(0)
	let no_input_message = "Wrong call: "^program_name^"bin/verify_signature <file : string> <signature-of-file : string> "

	let _ =
		try 
		(
			let file_name = Sys.argv.(1) in (* Load first file *)
			let signature_file = Sys.argv.(2) in (* Load second signature *)
			let signature = load_signature signature_file in 
			let satisfies = ring_verify file_name signature in 
			let accepted_message = 
			(function | true -> print_string [green] "Accepted" | _ -> print_string [red] "Rejected") in 
				(* printf "Signature(%s, %s): " (file_name) (signature_file) ; *)
				accepted_message (satisfies) ; print_newline();
				
		)
		with 
			| Sys_error(e) -> printf "%s\n" (e);
			| Failure(e) -> printf "%s: While loading signature\n" (e);
			| Invalid_argument(e) -> printf "%s\n" (no_input_message);