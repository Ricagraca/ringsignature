
	open Array
	open List
	open Sys
	open Unix
	open Printf 
	open String 
	open Random
	open Yojson 
	open Yojson.Basic
	open Yojson.Basic.Util
	open Cryptokit 
	open RSA 
	open CryptokitBignum 

	(* Size of key in bits *)
	let two = of_int 2 
	let key_size = 1024

	(* Seting some global values *)
	let key_dir = "keys/"
	let pub_key_dir = "keys/public/"
	let priv_key_dir = "keys/private/"

	(* Output messages *)
	let no_priv_file_message = "Selected a directory of private keys!"
	let no_pub_file_message = "Selected a directory of public keys!"

	(* Selected hash function *)
	let hash_function = Hash.sha256()

	(* Some usefull functions *)
	let hex s = transform_string (Hexa.decode()) s
	let hexbytes s = Bytes.of_string (hex s)
	let tohex s = transform_string (Hexa.encode()) s

	(* Calculate the hash of a file *)
	let hash_of_file (file_name : string) : (string) =
		let input_channel = open_in file_name in
		let hash_in_bytes = hash_channel hash_function input_channel in 
		hash_in_bytes


	(* type direction = Encrypt | Decrypt *)
	let aes (byte_key : string) (direct : Cipher.direction) (byte_string : string) = 
		let aes_size_of_key = Cipher.aes byte_key direct ~mode: (Cryptokit.Cipher.CFB 8) in 
		transform_string aes_size_of_key byte_string 


	(* Chacha algorithm *)
	let chacha (byte_key : string) (byte_string : string) = 
		let chacha_size_of_key = Cipher.chacha20 byte_key Cipher.Encrypt in 
		transform_string chacha_size_of_key byte_string 


	(* Read private rsa key from file file_name *)
	let read_priv_key (json_key : json) : (int * key) = 
		let list_of_key = 
		match json_key with 
		| `List x -> x  
		|  _      -> []
		in 
		try
			let name = nth list_of_key 0 in 
			let size = nth list_of_key 1 in 
			let n    = nth list_of_key 2 in 
			let e    = nth list_of_key 3 in 
			let d    = nth list_of_key 4 in 
			let p    = nth list_of_key 5 in 
			let q    = nth list_of_key 6 in 
			let dp   = nth list_of_key 7 in 
			let dq   = nth list_of_key 8 in 
			let qinv = nth list_of_key 9 in 
			(
			(to_int name),
				{
				size=(to_int size);
				n=(to_string n);
				e=(to_string e);
				d=(to_string d);
				p=(to_string p);
				q=(to_string q);
				dp=(to_string dp);
				dq=(to_string dq);
				qinv=(to_string qinv)
				}
			)
		with Failure(e) -> printf "%s\n" (no_priv_file_message); exit(0)


	(* Read private rsa key from file file_name *)
	let read_priv_key_of_file (file_name : string) : (int * key) = 
		read_priv_key (from_file file_name)


	(* Read public rsa key from file file_name *)
	let read_pub_key (json_key : json) : (int * key) = 
		let list_of_key = 
		match json_key with 
		| `List x -> x  
		|  _      -> []
		in 
		try
			let name = nth list_of_key 0 in 
			let size = nth list_of_key 1 in 
			let n    = nth list_of_key 2 in 
			let e    = nth list_of_key 3 in 
			(
			(to_int name),
				{
				size=(to_int size);
				n=(to_string n);
				e=(to_string e);
				d="";
				p="";
				q="";
				dp="";
				dq="";
				qinv=""
				}
			)
		with Failure(e) -> printf "%s\n" (no_pub_file_message); exit(0)


	(* Read public rsa key from file file_name *)
	let read_pub_key_of_file (file_name : string) : (int * key) = 
		read_pub_key (from_file file_name)


	(* Read public rsa keys from directory dir_name *)
	let read_pub_keys_of_directory (dir_name : string) : (int * key) array = 
		let files = Sys.readdir dir_name in
		let get_full_path file = dir_name^"/"^file in 
		let array_of_keys = Array.map (
			fun file -> 
			read_pub_key_of_file (get_full_path file) 
		) (files) in 
		let sorting_predicate = (fun a b -> (fst a) - (fst b)) in 
		let () = Array.sort sorting_predicate array_of_keys in 
		array_of_keys


	(* Returns a json object of the public key *)
	let json_of_pub (key : key) (name : int) : json = 
		let {size;n;e} = key in 
		let json_int_of_name = `Int name in 
		let json_int_of_size = `Int size in 
		let json_string_of_n = `String (n) in 
		let json_string_of_e = `String (e) in 
		let list_of_pub = 
			[
				json_int_of_name; json_int_of_size; 
				json_string_of_n; json_string_of_e;
			] in 
		let json_pub = `List list_of_pub in 
		json_pub 


	(* Returns a json object of the private key *)
	let json_of_priv (key : key) (name : int) : json = 
		let {size;n;e;d;p;q;dp;dq;qinv} = key in 
		let json__int__of__name = `Int name in 
		let json__int__of__size = `Int size in 
		let json__string__of__n = `String (n) in 
		let json__string__of__e = `String (e) in 
		let json__string__of__d = `String (d) in 
		let json__string__of__p = `String (p) in 
		let json__string__of__q = `String (q) in 
		let json_string__of__dp = `String (dp) in 
		let json_string__of__dq = `String (dq) in 
		let json_string_of_qinv = `String (qinv) in 
		let list_of_priv = 
			[	
				json__int__of__name; json__int__of__size; 
				json__string__of__n; json__string__of__e; 
				json__string__of__d; json__string__of__p; 
				json__string__of__q; json_string__of__dp; 
				json_string__of__dq; json_string_of_qinv;
			] in 
		let json_priv = `List list_of_priv in 
		json_priv 


	(* Writes private rsa key to {file_name}.pk *)
	let write_priv_key_to_file ((key,name) : (key * int)) (file_name : string) : unit = 
		let priv_name = concat "." [file_name;"sk"] in 
		let json_priv = json_of_priv key name in 
		to_file priv_name json_priv


	(* Writes public rsa key to {file_name}.pk *)
	let write_pub_key_to_file ((key,name) : (key * int)) (file_name : string) : unit = 
		let pub__name = concat "." [file_name;"pk"] in 
		let json__pub = json_of_pub key name in 
		to_file pub__name json__pub


	let padd_file_name_with_zero file_name (final_size : int) = 
		let size = length file_name in 
		let number_zeros = final_size-size in 
			let get_char index = get file_name index in 
			let func index = 
			match index < number_zeros with 
			| true -> '0' 
			| _    -> get_char (index - number_zeros) in 
		String.init final_size func


	(* Save key (both private and public) *)
	let save_key (key : key) (number_of_key : int) (file_name_size : int) : unit =
		let id_string = string_of_int number_of_key in 
		let padded_id = padd_file_name_with_zero id_string file_name_size in 
		let pub_file_name = pub_key_dir^padded_id in  
		let priv_file_name = priv_key_dir^padded_id in  
		write_pub_key_to_file (key,number_of_key-1) (pub_file_name);
		write_priv_key_to_file (key,number_of_key-1) (priv_file_name)


	(* Create and add number_of_keys keys to the respective directories *)
	let rec save_all_keys (number_of_key : int) (file_name_size : int) : unit = 
		match number_of_key with 
		| 0   -> ()
		| _   -> save_key (new_key key_size) number_of_key file_name_size; 
		save_all_keys (number_of_key-1) file_name_size


	(* Remove all files in directory *)
	let remove_files (dir : string) : unit = 
		let files = Sys.readdir dir in
		Array.iter (fun f -> remove (dir^f)) (files)


	(* Calculate B = 2^{b} *)
	let big_pow_2 (num : int) : CryptokitBignum.t = 
		let rec pow n acc = 
		match n with 
		| 0 -> acc
		| _ -> pow (n-1) (mult two acc)
	in pow num one


	(* Xor of two chars *)
	let xor_of_char a b = 
	  	let int_a = int_of_char a in 
	  	let int_b = int_of_char b in 
	  	let xor_a_b = int_a lxor int_b in 
	  	char_of_int xor_a_b;;


	(* Xor of two strings *)
    let xor_string (a : string) (b : string) : string = 
    	let len_a = String.length a in 
    	let len_b = String.length b in 
    	let size  = max len_a len_b in 
    	let init_func = (fun i -> 
    		if i >= len_a then 
    			String.get b i 
    		else (
    			if i >= len_b then 
    				String.get a i
    			else
    				xor_of_char (String.get a i) (String.get b i)
    		)
    	) in 
    	String.init size init_func;;


    (* Init for list *)
    let array_to_list function_ array_ =   
    	let size = Array.length array_ in 
    	let rec iterate i acc = 
    		match i < 0 with
    		| true  -> acc
    		| false -> iterate (i-1) (function_ (array_.(i))::acc) in 
    	iterate (size-1) []




