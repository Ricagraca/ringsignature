
	open Sys
	open List
	open Unix
	open Printf 
	open String 
	open Random
	open Yojson 
	open Yojson.Basic
	open Yojson.Basic.Util
	open Cryptokit 
	open CryptokitBignum 
	open RSA 
	open Utility

	(* Name of the program *)
	let program_name = Sys.argv.(0)

	(* Set permission *)
	let permission = 0o777 

	(* Value of error messages *)
	let no_input_message = "Wrong call: "^program_name^" <number-keys:int> "

	(* Start configuration *)
	let setup number_of_keys = 

		(* Create directories *)
		if not (file_exists (key_dir)) then mkdir key_dir permission;
		if not (file_exists (pub_key_dir)) then mkdir pub_key_dir permission;
		if not (file_exists (priv_key_dir)) then mkdir priv_key_dir permission;

		(* Remove old files if exist *)
		remove_files (pub_key_dir);
		remove_files (priv_key_dir);

		let file_name_size = length (string_of_int (number_of_keys)) in 
		save_all_keys number_of_keys file_name_size


	let _ = 
		(* Number of keys = ring size *)
		try 
			let ring_size = int_of_string (Sys.argv.(1)) in 
			init ring_size; (* Starts with seed ring_size *)
			setup ring_size (* Start program *)
		with
		| Invalid_argument(e) -> printf "%s\n" (no_input_message);
		| Failure(e) -> printf "%s\n" (no_input_message);
		| Sys_error(e) -> printf "%s\n" (no_input_message); 
